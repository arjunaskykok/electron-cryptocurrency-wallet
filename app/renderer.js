const Web3 = require('web3');

const web3 = new Web3('http://localhost:7545');

const submitButton = document.querySelector('#submit-button');
const senderInput = document.querySelector('#sender-input');
const destinationInput = document.querySelector('#destination-input');
const amountInput = document.querySelector('#amount-input');

submitButton.addEventListener('click', () => {
    const senderAccount = senderInput.value;
    const destinationAccount = destinationInput.value;
    const amount = amountInput.value;

    const rawTx = {
        from: senderAccount,
        to: destinationAccount,
        value: web3.utils.toWei(amount, 'ether')
    };

    web3.eth.sendTransaction(rawTx)
    .then((receipt) => {
        alert('Transfer is successful!');
        senderInput.value = '';
        destinationInput.value = '';
        amountInput.value = '';
    });
});